def add(num1,num2)
  num1 + num2
end

def subtract(num1,num2)
  num1 - num2
end

def sum(arr)
  arr.reduce(0) {|acc,el| acc + el}
end

def multiply(num1,num2,num3=1)
  num1 * num2 * num3
end

def power(num1, num2)
  num1 ** num2
end

def factorial(num)
  (1..num).to_a.reduce(1) {|acc,el| acc * el}
end
