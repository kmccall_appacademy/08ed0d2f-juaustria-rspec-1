def echo (word)
   word
end

def shout (word)
  word.upcase
end

def repeat (string, num = 2)
  word_container = []
  num.times { word_container << string }
  word_container.join(" ")
end

def start_of_word (word,range = 0)
  word.chars[0..(range-1)].join
end

def first_word (string)
  string.split(" ")[0]
end

def titleize (string)
  little_words = ['a', 'an', 'and', 'but','for', 'nor','or','over','the']
  string.split(" ").map.with_index do |word,idx|
    if little_words.include?(word) != true || idx == 0
      word.capitalize
    else
      word
    end
  end
  .join(" ")
end
