def translate(string)
  string.split(" ").map do |word|
    pig_latinfy(word)
  end
  .join(" ")
end

def pig_latinfy(string_base, to_append = ["ay"])

  if string_base[0] == 'q' && string_base[1] == 'u'
    if to_append.length == 1
      to_append = [string_base[0..1], to_append[0]]
    else
      to_append[0] += string_base[0..1]
    end
    string_base = string_base[2..(string_base.length - 1)]
    pig_latinfy(string_base, to_append)
  end

  if "aeiouAEIOU".include?(string_base[0]) == false
    if to_append.length == 1
      to_append = [string_base[0], to_append[0]]
    else
      to_append[0] += string_base[0]
    end
    string_base = string_base[1..(string_base.length - 1)]
    pig_latinfy(string_base, to_append)
  else
    string_base + to_append.join
  end

end
